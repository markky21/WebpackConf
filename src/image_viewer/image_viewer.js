import './image_viewer.scss';
import big from '../assets/_IMG3263.jpg';
import small from '../assets/_IMG3276.jpg';

const image = document.createElement('img');
image.src = small;

document.body.appendChild(image);


const bigImage = document.createElement('img');
bigImage.src = big;

document.body.appendChild(bigImage);